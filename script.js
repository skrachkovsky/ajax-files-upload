/**
 * @author Sergei Krachkovsky
 * @email  sergei.krachkovsky@hotmail.com
 */

Array.prototype.getKeys = function () {
    var k = [];
    for (var i = 0, j; j = this[i]; i++) {
        k.push(i);
    }
    return k;
};
Array.prototype.end = function () {
    return this.slice(0).pop();
};

var Upload = {
    files: [],
    urls: [],
    form: 'form',
    dropTargetSelector: '.well',
    uploadsStack: [],
    main: function (form) {
        Upload.form = form;
        if (window.FormData === undefined) {
            alert('not supported')
        }
        var dropTarget = $(Upload.dropTargetSelector);
        var withinEnter = false;
        dropTarget.bind('dragenter', function (evt) {
            evt.preventDefault();
            withinEnter = true;
            setTimeout(function () {
                withinEnter = false;
            }, 0);
            $(this).addClass('js-dropzone');
        });
        dropTarget.bind('dragover', function (evt) {
            evt.preventDefault();
        });
        dropTarget.bind('dragleave', function (evt) {
            if (!withinEnter) {
                $(this).removeClass('js-dropzone');
            }
            withinEnter = false;
        });
        dropTarget.bind('drop', function (evt) {
            $(this).removeClass('js-dropzone');
            withinEnter = false;
            evt.preventDefault();
            Upload.addFiles(evt.originalEvent.dataTransfer.files);
        });
        $(document).on('click', form + ' .remove', function () {
            if ($(this).data('key') !== undefined) {
                if ($(this).data('type') == 'url') {
                    delete Upload.urls[$(this).data('key')];
                } else {
                    delete Upload.files[$(this).data('key')];
                }
                $(this).parent().parent().remove();
            }
            return false;
        });
        $(form).find('.add-files').click(function (e) {
            if (e.which != 1) {
                return;
            }
            $(form).find('input[type="file"]').click();
        });
        $(form).find('input[type="file"]').change(function () {
            Upload.addFiles(this.files);
            $(this).val('');
        });
        $(form).submit(function () {
            Upload.startUpload(Upload.files.slice(0), Upload.urls.slice(0));
            Upload.files = [];
            Upload.urls = [];
            return false;
        });
    },
    startUpload: function (files, urls) {
        if (files.length > 0 || urls.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append("file[]", files[i]);
                }
                var pid = parseInt(Math.random() * 1e11);
                Upload.uploadsStack.push(1);
                $.ajax({
                    url: $(Upload.form).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    data: data,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener('progress', function(evt) {
                            if (evt.lengthComputable) {
                                $(Upload.form + ' #p-' + pid + ' .progress-bar')
                                    .css('width', parseInt(evt.loaded * 100 / evt.total) + '%')
                                    .text(parseInt(evt.loaded * 100 / evt.total) + '%');
                            }
                        }, false);
                        return xhr;
                    },
                    beforeSend: function () {
                        var uf = '';
                        $(Upload.form + ' .files li .name').each(function () {
                            uf += '<p>' + $(this).text() + '</p>';
                        });
                        $(
                            '<div class="progress-item well well-sm" id="p-' + pid + '">' +
                                '<div class="progress">' +
                                    '<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<div class="progress-files">' + uf + '</div>' +
                            '</div>'
                        ).appendTo(Upload.form + ' .progress-block');
                        $(Upload.form + ' .files li').remove();
                    },
                    success: function (r) {
                        alert('ok!');
                    },
                    complete: function () {
                        Upload.uploadsStack.shift();
                        if (Upload.uploadsStack.length == 0) {
                            // finish fn
                        }
                        $(Upload.form + ' #p-' + pid).remove();
                    }
                });
            }
        } else {
            alert('error')
        }
    },
    addFiles: function (fs, isUrl) {
        var ns = [];
        for (var i = 0, f; f = fs[i]; i++) {
            if (!isUrl && f.type.indexOf('image/') !== 0) {
                ns.push(f.name);
                continue;
            }
            var lastId = 0;
            if (isUrl) {
                Upload.urls.push(f);
                lastId = Upload.urls.getKeys().end();
            } else {
                Upload.files.push(f);
                lastId = Upload.files.getKeys().end();
            }
            $(
                '<li>' +
                    '<span class="rem btn btn-xs btn-danger"><span data-type="' + (isUrl ? 'url' : 'file') + '" data-key="' + lastId + '" class="remove">remove</span></span>\n' +
                    '<span class="name">' + f.name + '</span>\n<span class="size">' + Upload.bytesToSize(f.size) + '</span>'+
                '</li>'
            ).appendTo(Upload.form + ' .files');
        }
        if (ns.length) {
            alert('error')
        }
    },
    bytesToSize: function (bytes) {
        var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i == 0) return bytes + ' ' + sizes[i];
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }
};